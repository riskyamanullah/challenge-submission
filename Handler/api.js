const { user } = require("../DBuser/DBuser");
const { success, fail } = require("../response");

module.exports = {
    handleLogin: (req, res) => {
        res.redirect("/");
    },
    handleDetailUser: (req, res) => {
        const { email } = req.params;
        const user = user.find((u) => u.email === email);
        if (user) {
            success(res, user);
            return;
        }
        fail(res, `user with email ${email} not exist`);
        return;
    },
};
