const path = require("path");
module.exports = {
    handleLogin: (req, res) => {
        res.render("login");
    },
    handleHomePage: (req, res) => {
        res.sendFile('Challenge CH 3_Risky Amanullah.html', {
            root: path.join(__dirname, '../Sources')
        });
    },
    handleSuitPage: (req, res) => {
        res.sendFile('Challenge CH 4_Risky Amanullah.html', {
            root: path.join(__dirname, '../Sources')
        });
    },
};
