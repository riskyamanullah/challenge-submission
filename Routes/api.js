const express = require("express");
const path = require("path");
const { checkUserMiddleware } = require("../Middleware");
const { handleLogin, handleDetailUser } = require("../Handler");
const { body } = require("express-validator");
const router = express.Router();

router.use(express.static(path.join(__dirname, "Sources")));

router.post(
    "/login",
    [
        body("email").isEmail(),
        body("password").isLength({ min: 6 }),
    ],
    checkUserMiddleware,
    handleLogin
);
router.post("/user/:email", handleDetailUser);
module.exports = router;
