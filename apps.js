const express = require("express");
const morgan = require('morgan');
const path = require("path");
const { Sequelize } = require("sequelize");
require('dotenv').config();
const app = express();
const port = 5000;
const { handleSuitPage, handleHomePage, handleLogin } = require("./Handler/handles");
const { modelch6 } = require('./models')
const sequelize = new Sequelize(
    'postgres://postgres:querida@127.0.0.1:5100/postgres'
);
const tradgamesusers = sequelize.define('tradgamesusers',{
  id: {
    type: Sequelize.STRING,
    primaryKey: true
  },
  value: {
    type: Sequelize.STRING,
  }
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
      })
    .catch(error => {
        console.error('Unable to connect to the database:', error);
      });


app.use(express.json());
app.use(morgan('dev'));
app.use(express.urlencoded({extended:false}));
app.set("view engine", "ejs");
app.set("sources", path.join(__dirname, "sources"));
app.use(express.static("sources"));
app.get("/", handleLogin);
app.get("/home", handleHomePage);
app.get("/play", handleSuitPage);
app.listen(port, ()=> console.log('http://localhost:${port}'))