'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class usergamehistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  usergamehistory.init({
    score: DataTypes.INTEGER,
    playtime: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'usergamehistory',
  });
  return usergamehistory;
};