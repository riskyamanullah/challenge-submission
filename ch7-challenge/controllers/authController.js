// controllers/authController.js
const { User } = require("../models");
const passport = require("../lib/passport");

function format(user){
  const {id, username} = user
  return {
    id,
    username,
    accessToken: user.generateToken()
  }
}
module.exports = {
  register: (req, res, next) => {
    // Kita panggil static method register yang sudah kita buat tadi
    User.register(req.body)
      .then(() => {
        res.redirect("/login");
      })
      .catch((err) => next(err));
  },
  login: passport.authenticate('local', { 
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true // Untuk mengaktifkan express flash
  }),
  login: (req, res) => {
    User.authenticate (req.body)
    .then(user => {
      res.json(
        format(user)
      )
    })
  }
};


// const router = require('express').Router()
//   // Controllers
// // const auth = require('./controllers/authController' )
//  // Homepage
// router.get('/', (req, res) => res.render('index'))
//  // Register Page
// router.get('/register', (req, res) => res.render('register'))
// router.post('/register', auth.register)

// login: passport.authenticate('local', {
//   successRedirect: '/',
//   failureRedirect: '/login',
//   failureFlash: true // Untuk mengaktifkan express flash
// });

// router.post('/login', auth.login)

// module.exports = router;
