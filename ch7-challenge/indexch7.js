//async example

// const fs = require("fs");
// const option = { encoding: "utf-8" };

// const callback = (err, data) => {
//   console.log("Aku muncul kedua");
//     if (err) return console.error("Error:", err.message);
//     console.log("Isi File:", data);
// };
// fs.readFile("latihan.txt", option, callback);

// console.log("Aku muncul pertama");

// sync example

const fs = require("fs");
const option = { encoding: "utf-8" };

console.log("Aku muncul pertama");
const data = fs.readFileSync("latihan.txt", option);
console.log("Isi File:", data);

setTimeout(() => console.log('keluar setelah 2 detik'), 2000);
setTimeout(() => console.log('aku selanjutnya'), 3000);

// setInterval(() => {
//     console.log('selanjutnya')
// }, 1000);

console.log("aku muncul ketiga")

