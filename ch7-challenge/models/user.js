'use strict';
const {
  Model
} = require('sequelize');

const bycrypt = require ('bcrypt')
const jwt = require ('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    static #encrypt = (password) => bycrypt.hashSync (password,10)
    static register = ({username, password}) => {
      const encryptedPassword = this.#encrypt (password)
      return this.create({username, password:encryptedPassword})
    }
  checkPassword = password => bycrypt.compareSync(password, this.password)
  // generating token method
  generateToken = () => {
    const payload ={
      id: this.id,
      username: this.username
    }
    const rahasia = 'ya rahasia ini'
    const token = jwt.sign (payload, rahasia)
    return token
    }
  static authenticate = async ({username, password}) => {
    try {
      const user = await this.findOne ({where: {username}})
      if (!user) return Promise.reject("user not found!")

      const isPasswordvalid = user.checkPassword(password)
      if (!isPasswordvalid) return Promise.reject("wrong password")

      return Promise.resolve(user)
    }
    catch(err) {
      return Promise.reject(err)
    }
  }
  };

  User.init({
    username: DataTypes.STRING,
    password: DataTypes.TEXT,
    role: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};