const router = require('express').Router()
const restrict = require('./middlewares/restrict');
const auth = require('./controllers/authController');

// Kalau mau tulis kode ini di file router.js
// Letakkan kode ini di bagian bawah router.get()

router.get('/', restrict, (req, res) => res.render('index'))
router.get('/login', passport.authenticate('local', {
successRedirect: '/',
failureRedirect: '/login',
failureFlash: true
}))
router.post('/login', auth.login)
router.get('/register', (req, res) => res.render('register'))
router.post('/register', auth.register)

module.exports = router;