const express = require('express')
const passport = require('./lib/passport')
const app = express()
// const router = require('./router/router')
const session = require('express-session')
const flash = require('express-flash')
const { PORT = 8000 } = process.env
const router = require('express').Router()

const auth = require('./controllers/authController')

// Pertama, setting request body parser
// (Ingat! Body parser harus ditaruh paling atas)
app.use(express.urlencoded({ extended: false }))

// Kedua, setting session handler
app.use(session({
    secret: 'Buat ini jadi rahasia',
    resave: false,
    saveUninitialized: false
}))

app.use(passport.initialize())
app.use(passport.session())

// Keempat, setting flash
app.use(flash())

// Kelima, setting view engine
app.set('view engine', 'ejs')

// // Keenam, setting router
app.use(router)
app.get('/', function(req, res){
    res.render('index.ejs');
  });
app.get('/api/login', function(req, res){
    res.render('login.ejs');
  });
app.post('/api/login', function(req,res){
  res.render('greeting.ejs');
});
app.get('/login', auth.login)
app.listen(PORT, () => {
console.log(`Server nyala di port ${PORT}`) })