import { useState, useRef, useEffect } from 'react';
import { Grid, TextField, Typography } from '@mui/material';
import './index.css'
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import PersonSearchIcon from '@mui/icons-material/PersonSearch';
import Button from '@mui/material/Button';

const SearchPlayer =() => {
  const theme = createTheme();
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [experience, setExperience] = useState('');
  const [level, setLevel] = useState('');
  const [submittedData, setSubmittedData] = useState({
      username: '',
      email: '',
      experience: '',
      level: '',
  });
  const formWrapperElem = useRef(null);

  useEffect(() => {
    console.log('functional componentDidMount')

    return () => {
      console.log('functional componentWillUnmount')
    }
  }, [])
  
  const handleSubmit = () => {
    setSubmittedData({
        username: username,
        email: email,
        experience: experience,
        level: level,
    })
}
  const handleUsername = (data) => {
    setUsername(data.target.value)
}

const handleEmail = (data) => {
  setEmail(data.target.value)
}

const handleExperience = (data) => {
    setExperience(data.target.value)
}

const handleLevel = (data) => {
    setLevel(data.target.value)
}

  
  return <div id="SearchPlayer-wrapper" ref={formWrapperElem}>

  <ThemeProvider theme={theme}>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
          >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <PersonSearchIcon />
        </Avatar>

        <Typography component="h1" variant="h5">
          SEARCH ACCOUNT
        </Typography>

        <Box component="form" sx={{ mt: 3 }}>
          <Grid container spacing={2}>

            <Grid item xs={12}>
              <TextField 
                required fullWidth 
                onChange={handleUsername}
                value={username} 
                id="yourName" 
                label="Your Name" 
                variant="outlined" />
              </Grid>

            <Grid item xs={12}>
              <TextField 
              required fullWidth 
              onChange={handleEmail}
              value={email} 
              id="email" 
              label="Your Email" 
              type="email"  
              variant="outlined" />
              </Grid>

            <Grid Grid item xs={12}>
              <TextField 
              required fullWidth 
              onChange={handleExperience}
              value={experience} 
              id="experience" 
              label="Experience" 
              type="text" 
              variant="outlined" />
              </Grid>

            <Grid item xs={12}>
              <TextField 
              required fullWidth 
              onChange={handleLevel}
              value={level} 
              id="level" 
              label="Level" 
              type="text" 
              variant="outlined" />
              </Grid>

            

            <Button
              onClick={handleSubmit}
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2, ml: 2 }}
              >
              SUBMIT
            </Button>

            <Grid item xs={6}>
                <div>{`Username: ${submittedData.username}`}</div>
                <div>{`Email: ${submittedData.email}`}</div>
                <div>{`Password: ${submittedData.experience}`}</div>
                <div>{`Level: ${submittedData.level}`}</div>
            </Grid>

            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  </div>
}

  export default SearchPlayer;