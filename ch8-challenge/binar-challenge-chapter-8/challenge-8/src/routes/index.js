// import { lazy } from 'react';
// import Wrapper from '../components/RatingComponentClassComponent/wrapper';

import Registration from '../components/Registration';
import SearchPlayer from '../components/SearchPlayer';
// Not Lazy Load (10.73s, 190 kB)
// import Counter from '../components/Counter';
// import CreateNewPlayer from '../components/CreateNewPlayer';
// import RatingComponent from '../components/RatingComponent'
// import Suit from '../components/Suit';
// import SearchPlayerClassComponent from '../components/SearchPlayerClassComponent';

// Lazy Loaded (12.63s, 190 kB)
// const Counter = lazy(() => import('../components/Counter'));
// const CounterClassComponent = lazy(() => import('../components/CounterClassComponent'));
// const CreateNewPlayer = lazy(() => import('../components/CreateNewPlayer'));
// const RatingComponent = lazy(() => import('../components/RatingComponent'));
// const Suit = lazy(() => import('../components/Suit'));
// const SearchPlayerClassComponent = lazy(() => import('../components/SearchPlayerClassComponent'));

const routes = [
    {
        path: '/',
        component: <Registration />,
        exact: true,
    },
    {
        path: '/search',
        component: <SearchPlayer />,
        exact: false,
        },
    // {
    //     path: '/rating',
    //     component: <RatingComponent
    //         stars={3}
    //         bebasdeh={"hehe"}
    //     />,
    //     exact: false,
    // },
    // {
    //     path: '/rating-class',
    //     component: <Wrapper />,
    //     exact: false,
    // },
    // {
    //     path: '/suit',
    //     component: <Suit />,
    //     exact: false,
    // },
    // {
    //     path: '/counter',
    //     component: <Counter />,
    //     exact: false,
    // },
    // {
    //     path: '/counter-class',
    //     component: <CounterClassComponent />,
    //     exact: false,
    // },
    // {
    //     path: '/',
    //     component: <CreateNewPlayer />,
    //     exact: true,
    // },
    // {
    //     path: '/search',
    //     component: <SearchPlayerClassComponent />,
    //     exact: false,
    // },
    // {
    //     path: '*',
    //     component: "Not Found",
    //     exact: false,
    // },
]

export default routes;