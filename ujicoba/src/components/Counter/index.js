import { useState } from 'react';
import { Fab } from '@mui/material';
// PascalCase
const Counter = (props) => {
    const [count, setCount] = useState(0);

    // 5 nextnya 1. 1 sebelumnya 5
    const handleAdd = () => {
        if (count === 5) {
            setCount(1)
        } else {
            setCount(count + 1)
        }
    }
    const handleSub = () => {
        if (count <=0) {
            setCount(0)
        } else {
            setCount(count - 1)
        }
    }
    return <>
        <h2>{count}</h2>
        <Fab
            onClick={handleAdd}
            color="primary"
            aria-label="add"
        >
            Tambah
        </Fab>
        <Fab
            onClick={handleSub}
            color="primary"
            aria-label="add"
            disabled={count === 0}
        >
            Kurang
        </Fab>
    </>
}

export default Counter;