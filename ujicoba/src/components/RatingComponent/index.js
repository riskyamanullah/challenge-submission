import { useState, useEffect } from 'react';
import { Rating, Typography } from '@mui/material';
// PascalCase
const RatingComponent = (props) => {
    const { stars } = props;
    const [starCount, setStarCount] = useState(stars);
    const [loading, setLoading] = useState(true);

    const handleCallApi = () => {
        setLoading(true)
        setTimeout(() => {
            setStarCount(5)
            setLoading(false)
        }, 3000)
    };

    const handleCheckState = () => {
        console.log('starCount', starCount)
    };

    useEffect(handleCallApi, [])
    useEffect(handleCheckState, [starCount])

    return <div>
        <Typography component="legend">{loading ? 'loading' : 'Controlled'}</Typography>
        <Rating
            name="simple-controlled"
            value={starCount}
            onChange={(event, newValue) => {
                setStarCount(newValue);
            }}
        />
    </div>
}

export default RatingComponent;